package Practica05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Principal {
    public static void main(String[] args) {
        int opcionPrim = 0, opcionSec = 0;
        Auto auto = new Auto(1, "Suzuki", 5, 30000.00);
        do {
            opcionPrim = menuPrim();
            switch (opcionPrim) {
                case 1: //XML
                    do {
                        opcionSec = menuSec();
                        switch (opcionSec) {
                            case 1: //Imprimir
                                System.out.println(auto.toXML());
                                break;
                            case 2: //convertir
                                System.out.println("Cambio de datos");
                                String nuevoAuto = "<Auto>" +
                                        "    <id>1</id>" +
                                        "    <modelo>Mustang</modelo>" +
                                        "    <costo>35000.00</costo>" +
                                        "    <numeroLLantas>6</numeroLLantas>" +
                                        "</Auto>";
                                auto.fromXML(nuevoAuto);
                                System.out.println(auto.toXML());
                                break;
                            case 3: //salir
                                System.out.println("Volviendo al menu principal");
                                break;
                            default:
                                break;
                        }
                    } while (opcionSec != 3);
                    break;
                case 2: //JSON
                    do {
                        opcionSec = menuSec();
                        switch (opcionSec) {
                            case 1: //Imprimir
                                System.out.println(auto.toJson());
                                break;
                            case 2: //convertir
                                String nuevoAuto = "{" +
                                        "\"id\":5," +
                                        "\"modelo\":\"Ford\"," +
                                        "\"numeroLLantas\":6," +
                                        "\"costo\":60000.00" +
                                        "}";
                                auto.fromJson(nuevoAuto);
                                System.out.println(auto.toJson());
                                break;
                            case 3: //salir
                                System.out.println("Volviendo al menu principal");
                                break;
                            default:
                                break;
                        }
                    } while (opcionSec != 3);
                    break;
                case 3: //salir
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("Opcion no valida");
            }
        } while (opcionPrim != 3);

    }

    public static int menuPrim () {
        int opcion = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. XML");
        System.out.println("2. JSON");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }

    public static int menuSec() {
        int opcion = 0;
        BufferedReader scan  = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. Imprimir");
        System.out.println("2. Convertir");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }

}


