package Practica05;

public class Auto {
    private Integer id;
    private String modelo;
    private Integer numeroLLantas;
    private Double costo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Integer getNumeroLLantas() {
        return numeroLLantas;
    }

    public void setNumeroLLantas(Integer numeroLLantas) {
        this.numeroLLantas = numeroLLantas;
    }

    public Double getCosto() {
        return costo;
    }

    public void setCosto(Double costo) {
        this.costo = costo;
    }

    public Auto(Integer id, String modelo, Integer numeroLLantas, Double costo) {
        this.id = id;
        this.modelo = modelo;
        this.numeroLLantas = numeroLLantas;
        this.costo = costo;
    }

    public Auto() {
    }

    @Override
    public String toString() {
        return "Auto{" +
                "id=" + id +
                ", modelo='" + modelo + '\'' +
                ", numeroLLantas=" + numeroLLantas +
                ", costo=" + costo +
                '}';
    }

    public String toXML () {
        String xml = "<Auto>";
        xml += "<id>" + getId() + "</id>";
        xml += "<modelo>" + getModelo() + "</modelo>";
        xml += "<numeroLLantas>" + getNumeroLLantas() + "</numeroLLantas>";
        xml += "<costo>" + getCosto() + "</costo>";
        xml += "</Auto>";

        return xml;
    }

    public void fromXML (String json) {
        int inicio, fin;
        String dato;

        inicio = json.indexOf("<id>") + ("<id>") .length();
        fin = json.indexOf("</id>");
        dato = json.substring(inicio, fin);
        setId(Integer.parseInt(dato));

        inicio = json.indexOf("<modelo>") + ("<modelo>") .length();
        fin = json.indexOf("</modelo>");
        dato = json.substring(inicio, fin);
        setModelo(dato);

        inicio = json.indexOf("<costo>") + ("<costo>") .length();
        fin = json.indexOf("</costo>");
        dato = json.substring(inicio, fin);
        setCosto(Double.parseDouble(dato));

        inicio = json.indexOf("<numeroLLantas>") + ("<numeroLLantas>") .length();
        fin = json.indexOf("</numeroLLantas>");
        dato = json.substring(inicio, fin);
        setNumeroLLantas(Integer.parseInt(dato));
    }

    public String toJson () {
        String json = "{";

        json += "\"id\":" + getId() + ",";
        json += "\"modelo\": \"" + getModelo() + "\",";
        json += "\"numeroLLantas\":" + getNumeroLLantas() + ",";
        json += "\"costo\":" + getCosto();

        json += "}";

        return json;
    }

    public void fromJson (String json) {
        int inicio = 0, fin = 0;
        String dato = "", campo = "";

        json = json.replace("\"", "");
        json = json.replace("{", "");
        json = json.replace("}", "");

        String[] valores = json.split(",");

        for (String contenido: valores) {
            inicio = contenido.indexOf(":") + 1;
            fin = contenido.indexOf(":");

            dato = contenido.substring(inicio);
            campo = contenido.substring(0, fin);

            switch (campo) {
                case "id":
                    setId(Integer.parseInt(dato));
                    break;
                case "modelo":
                    setModelo(dato);
                    break;
                case "numeroLLantas":
                    setNumeroLLantas(Integer.parseInt(dato));
                    break;
                case "costo":
                    setCosto(Double.parseDouble(dato));
                    break;
            }

        }

    }

}

