package Cliente.Entity.Service;

import Cliente.Entity.Persona;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PersonaService {
    public static Persona cuestionario () {
        Persona persona = new Persona();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            persona.setId(0);
            System.out.println("Nombre:");
            persona.setNombre(reader.readLine());
            System.out.println("Apellido paterno:");
            persona.setPaterno(reader.readLine());
            System.out.println("Apellido materno:");
            persona.setMaterno(reader.readLine());
            System.out.println("Edad:");
            persona.setEdad(Integer.parseInt(reader.readLine()));
            System.out.println("Genero (M/F):");
            persona.setGenero(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return persona;
    }

    public static Integer preguntarID () {
        Integer id = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("¿Cual cliente esta buscando? (Coloque su ID)");
        try {
            id = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return id;
    }

    public static Persona actualizar (Persona persona) {
        Integer opcion = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Persona a actualizar: " + persona.toString());
        System.out.println("Que parte de la persona quieres actualizar?");
        System.out.println("1. Nombre:");
        System.out.println("2. Apellido paterno:");
        System.out.println("3. Apellido materno:");
        System.out.println("4. Edad:");
        System.out.println("5. Genero (M/F):");
        try {
            opcion = Integer.parseInt(reader.readLine());

            switch (opcion) {
                case 1:
                    System.out.print("Nuevo nombre: ");
                    persona.setNombre(reader.readLine());
                    break;
                case 2:
                    System.out.print("Nuevo apellido paterno: ");
                    persona.setPaterno(reader.readLine());
                    break;
                case 3:
                    System.out.print("Nuevo apellido materno: ");
                    persona.setMaterno(reader.readLine());
                    break;
                case 4:
                    System.out.print("Nueva edad: ");
                    persona.setEdad(Integer.parseInt(reader.readLine()));
                    break;
                case 5:
                    System.out.println("Nuevo genero (M/F): ");
                    persona.setGenero(reader.readLine());
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return persona;
    }
}
