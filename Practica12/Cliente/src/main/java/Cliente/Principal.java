package Cliente;

import Cliente.Controller.PrincipalController;
import Cliente.Entity.Gestores.GestorPersonas;
import Cliente.Entity.Persona;
import Cliente.Entity.Service.PersonaService;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Principal {
    public static void main(String[] args) {
        Integer puerto = 8000, opcion = 0;
        String host = "127.0.0.1", mensajeRecibido;
        Socket miCliente = null;
        PrintWriter writer = null;
        BufferedReader reader = null;
        InputStreamReader input = null;
        Gson gson = new Gson();

        try {
            miCliente = new Socket(host, puerto);
            input = new InputStreamReader(miCliente.getInputStream());
            reader = new BufferedReader(input);
            writer = new PrintWriter(miCliente.getOutputStream(), true);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            do {
                opcion = PrincipalController.menu();
                switch (opcion) {
                    case 1: //mostrar
                        writer.println("GET");
                        mensajeRecibido = reader.readLine();
                        GestorPersonas gestorPersonas = gson.fromJson(mensajeRecibido, GestorPersonas.class);
                        for (Persona iterador : gestorPersonas.getPersonas()) {
                            System.out.println("------------------------------------------------------------------" +
                                    "--------------");
                            System.out.println(iterador.toString());
                        }
                        System.out.println();
                        break;
                    case 2: // insertar
                        writer.println("POST");
                        Persona persona = PersonaService.cuestionario();
                        writer.println(gson.toJson(persona, Persona.class));
                        break;
                    case 3: // actualziar
                        writer.println("PUT");
                        writer.println(gson.toJson(PersonaService.preguntarID()));
                        Persona personaAct = gson.fromJson(reader.readLine(), Persona.class);
                        PersonaService.actualizar(personaAct);
                        writer.println(gson.toJson(personaAct, Persona.class));
                        break;
                    case 4: // Eliminar
                        writer.println("DELETE");
                        writer.println(gson.toJson(PersonaService.preguntarID()));
                        break;
                    case 5: // Salir
                        writer.println("SALIR");
                        break;
                    default:
                        System.out.println("Opcion no valida");
                        break;
                }

            }while (opcion != 5);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
