package Servidor.Entity.Service;

import Servidor.Entity.Persona;

import javax.print.DocFlavor;
import java.sql.*;
import java.util.ArrayList;

public class PersonaService {
    public static ArrayList<Persona> selectPersonas(Connection connection) {
        ArrayList<Persona> personas = new ArrayList<Persona>();
        try {
            String sql = "SELECT id_persona, nombre, paterno, materno, edad, genero FROM persona";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Persona persona = new Persona();
                persona.setId(rs.getInt(1));
                persona.setNombre(rs.getString(2));
                persona.setPaterno(rs.getString(3));
                persona.setMaterno(rs.getString(4));
                persona.setEdad(rs.getInt(5));
                persona.setGenero(rs.getString(6));
                personas.add(persona);
                persona = null;
            }
            stmt.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return personas;
    }

    public static void insertPersona(Connection connection, Persona persona) {
        PreparedStatement statement = null;
        String sql = "insert into persona(id_persona, nombre, paterno, materno, edad, genero) " +
                "VALUES (default, ?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(sql);

            statement.setString(1, persona.getNombre());
            statement.setString(2, persona.getPaterno());
            statement.setString(3, persona.getMaterno());
            statement.setInt(4, persona.getEdad());
            statement.setString(5, persona.getGenero());

            statement.executeUpdate();
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static Persona buscarXID(Connection connection, Integer ID) {
        Persona persona = new Persona();
        try {
            String sql = "SELECT id_persona, nombre, paterno, materno, edad, genero FROM persona where id_persona = " + ID;
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                persona.setId(rs.getInt(1));
                persona.setNombre(rs.getString(2));
                persona.setPaterno(rs.getString(3));
                persona.setMaterno(rs.getString(4));
                persona.setEdad(rs.getInt(5));
                persona.setGenero(rs.getString(6));
            }
            stmt.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return persona;
    }


    public static void updatePersona(Connection connection, Persona persona) {
        PreparedStatement statement = null;
        String sql = "UPDATE persona set nombre = ?, paterno = ?, materno = ?, " +
                "edad = ?, genero = ? where id_persona = " + persona.getId();
        try {
            statement = connection.prepareStatement(sql);

            statement.setString(1, persona.getNombre());
            statement.setString(2, persona.getPaterno());
            statement.setString(3, persona.getMaterno());
            statement.setInt(4, persona.getEdad());
            statement.setString(5, persona.getGenero());

            statement.executeUpdate();
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void eliminar (Connection connection, Integer id) {
        PreparedStatement statement = null;
        String sql = "delete from persona where id_persona = " + id;
        try {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
