package Servidor;

import Servidor.DB.DB;
import Servidor.Entity.Gestores.GestorPersonas;
import Servidor.Entity.Persona;
import Servidor.Entity.Service.PersonaService;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

public class Principal {
    public static void main(String[] args) {
        Integer puerto = 8000, id;
        Socket miCliente = null;
        ServerSocket miServidor = null;
        String mensajeRecibido = "";
        InputStreamReader input = null;
        BufferedReader reader = null;
        PrintWriter writer = null;
        Connection connection = null;
        Gson gson = new Gson();

        try {
            miServidor = new ServerSocket(puerto);
            System.out.println("Esperando conexión...");
            miCliente = miServidor.accept();
            System.out.println("Conexión aceptada");

            input = new InputStreamReader(miCliente.getInputStream());
            reader = new BufferedReader(input);
            writer = new PrintWriter(miCliente.getOutputStream(), true);

            connection = DB.conectar();

        } catch (IOException | SQLException e) {
            System.out.println("Fallo en el servidor");
            System.exit(1);
            e.printStackTrace();
        }

        try {
            do {
                mensajeRecibido = reader.readLine();
                switch (mensajeRecibido) {
                    case "GET": //Select
                        System.out.print("Entro al GET. ");
                        GestorPersonas gestorPersonas = new GestorPersonas();
                        gestorPersonas.setPersonas(PersonaService.selectPersonas(connection));
                        writer.println(gson.toJson(gestorPersonas, GestorPersonas.class));
                        System.out.println("Se envio la informacion");
                        break;
                    case "POST": // Insert
                        System.out.print("Entro al POST: ");
                        mensajeRecibido = reader.readLine();
                        Persona persona = gson.fromJson(mensajeRecibido, Persona.class);
                        PersonaService.insertPersona(connection, persona);
                        System.out.println("Se insertaron los datos con exito");
                        break;
                    case "PUT": // update
                        System.out.print("Entro al PUT: ");
                        id = gson.fromJson(reader.readLine(), Integer.class);
                        writer.println(gson.toJson(PersonaService.buscarXID(connection, id), Persona.class));
                        Persona personaAct = gson.fromJson(reader.readLine(), Persona.class);
                        PersonaService.updatePersona(connection, personaAct);
                        System.out.println("Se actualizo con exito");
                        break;
                    case "DELETE":
                        System.out.print("Entro al DELETE: ");
                        id = gson.fromJson(reader.readLine(), Integer.class);
                        PersonaService.eliminar(connection, id);
                        System.out.println("Eliminacion exitosa");
                        break;
                    case "SALIR":
                        System.out.println("Saliendo del servidor");
                        if (connection != null) {
                            miCliente.close();
                            miServidor.close();
                            connection.close();
                        }
                        break;
                    default:
                        System.out.println("Opcion no valida");
                        break;
                }
            }while (!mensajeRecibido.equals("SALIR"));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }
}
