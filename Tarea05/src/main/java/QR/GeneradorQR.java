package QR;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.awt.image.BufferedImage;

public class GeneradorQR {

    public BufferedImage crearQR(String datos, int ancho, int altura){
        BitMatrix matrix;
        BufferedImage imagen = null;
        Writer escritor = new QRCodeWriter();
        try {
            matrix = escritor.encode(datos, BarcodeFormat.QR_CODE, ancho, altura);

            imagen = new BufferedImage(ancho, altura, BufferedImage.TYPE_INT_RGB);

            for(int y = 0; y < altura; y++) {
                for(int x = 0; x < ancho; x++) {
                    int grayValue = (matrix.get(x, y) ? 0 : 1) & 0xff;
                    imagen.setRGB(x, y, (grayValue == 0 ? 0 : 0xFFFFFF));
                }
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }


        return imagen;
    }
}
