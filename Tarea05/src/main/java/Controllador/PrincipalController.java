package Controllador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrincipalController {
    public String preguntarTexto () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String texto = "";
        System.out.println("Coloque el texto que quiere convertir a PDF: ");
        try {
            texto = scan.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return texto;
    }
}
