import Controllador.PrincipalController;
import PDF.GeneradorPDF;
import QR.GeneradorQR;

import java.awt.image.BufferedImage;

public class Principal {

    private static PrincipalController controller = new PrincipalController();

    public static void main(String[] args) {
        GeneradorQR generadorQR = new GeneradorQR();

        BufferedImage image = generadorQR.crearQR(controller.preguntarTexto(), 300, 300);

        GeneradorPDF generadorPDF = new GeneradorPDF();

        generadorPDF.crearPDF(image);
    }
}
