package PDF;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class GeneradorPDF {
    public void crearPDF (BufferedImage image) {
        try {
            PDDocument document = new PDDocument();
            PDPage page = new PDPage(PDRectangle.A6);

            document.addPage(page);

            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            PDImageXObject imagePDF = JPEGFactory.createFromImage(document, image);

            contentStream.drawImage(imagePDF, 0,0, image.getWidth(), image.getHeight());

            contentStream.close();

            document.save("prueba.pdf");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
