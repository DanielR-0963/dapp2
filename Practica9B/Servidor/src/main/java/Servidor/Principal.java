package Servidor;

import Servidor.DB.DB;
import Servidor.Entity.Persona;
import Servidor.Entity.PersonaService;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;

public class Principal {
    public static void main(String[] args) {
        int puerto = 8000;
        Socket miCliente = null;
        InputStreamReader streamSocket = null;
        BufferedReader bufferedReader = null;
        Connection connection;
        try {
            ServerSocket miServidor = new ServerSocket(puerto);
            System.out.println("Esperando conexion...");
            miCliente = miServidor.accept();
            connection = DB.Conectar();
            streamSocket = new InputStreamReader(miCliente.getInputStream());
            bufferedReader = new BufferedReader(streamSocket);

            System.out.println("Esperando el mensaje...");
            String json = bufferedReader.readLine();

            Gson gson = new Gson();

            Persona persona = gson.fromJson(json, Persona.class);

            PersonaService.insertarPersona(connection ,persona);

            System.out.println(persona.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
