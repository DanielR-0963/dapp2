package Servidor.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PersonaService {
    public static void insertarPersona(Connection connection, Persona persona) {
        PreparedStatement statement = null;
        String sql = "insert into persona (id_persona, nombre, paterno, materno, edad, genero) values  (default,?,?,?,?,?)";
        try {
            statement = connection.prepareStatement(sql);

            statement.setString(1, persona.getNombre());
            statement.setString(2, persona.getPaterno());
            statement.setString(3, persona.getMaterno());
            statement.setInt(4, persona.getEdad());
            statement.setString(5, persona.getGenero());

            statement.executeUpdate();
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}

