import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pregunta07 {
    public static void main(String[] args) {
        int opcion = 0;

        do {
            opcion = menu();
            switch (opcion) {
                case 1: //iterativo
                    System.out.println("Tu resultado es: " + iterativoFacorial(pregunta()));
                    break;
                case 2: //recursivo
                    System.out.println("Tu resultado es: " + recursivoFactorial(pregunta()));
                    break;
                case 3: // Salir
                    System.out.println("Fin del programa");
                    break;
            }
        } while(opcion != 3);
    }

    private static int menu () {
        int opcion = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. Forma iterativa");
        System.out.println("2. Forma recursiva");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Fallo en leer la opcion");
        }
        return opcion;
    }

    private static int pregunta () {
        int factorial = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("De que numero quiere sacar factorial?");
        try {
            factorial = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return factorial;
    }

    private static int iterativoFacorial (int factorial) {
        int total = factorial;
        for (int i = 1; i < factorial; i++) {
            total = total * (factorial - i);
        }
        return total;
    }

    private static int recursivoFactorial (int factorial) {
        if (factorial == 0) {
            return 1;
        } else {
            return factorial * recursivoFactorial(factorial - 1);
        }
    }
}
