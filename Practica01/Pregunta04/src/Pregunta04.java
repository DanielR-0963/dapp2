public class Pregunta04 {

    /* UNIVERSIDAD IBEROAMERICANA
     * TSU en Software
     * Profesor:  Esp. Omar Vazquez Gonzalez
     * Alumno:       Ramírez Lázaro Luis Daniel
     * Asignatura:    Desarrollo de Aplicaciones II
     * Fecha:    23/01/2021
     * Programa:   Ejercicio
     * Descripción:
     */


    public static void main(String[] args) {
        System.out.println(sumatoria());
    }

    public static String sumatoria () {
        String sumatoriaFinal = "";
        int total = 0;
        for (int i = 1; i <= 5; i++) {
            total += (i * i);
        }
        return String.valueOf(total);
    }
}
