#include <iostream>

using namespace std;

/*	UNIVERSIDAD IBEROAMERICANA
 *	TSU en Software
 *	Profesor:	Esp. Omar VazquezGonzalez
 *	Alumno:		Juanito Perez
 *	Asignatura:	Desarrollo de Aplicaciones II
 *	Fecha:		23/01/2021
 *	Programa:   Ejercicio 1
 *	Descripcion:	Programa que muestre las tablas de multiplicar del 1 al 10
*/


int main() {
    for (int i = 1; i <= 10; i++) {
        for(int j = 1; j <= 10; j++) {
            int total = i * j;
            cout<< i << " x " << j << " = " << total << "\n";
        }
        cout<< "------------------------------------------'\n";
    }
    return 0;
}
