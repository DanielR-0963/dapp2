public class Pregunta06 {
    public static void main(String[] args) {
        int[]  x = new int[]{9, 6, 7, 2, 5, 3};

        int[] resultado = quicksort(x, 0, x.length - 1);

        for (int j : x) {
            System.out.println(j);
        }

    }

    public static int[] quicksort(int[] arreglo, int izq, int der) {

        int ancla = arreglo[izq];
        int i = izq;
        int j = der;
        int aux;

        while(i < j){
            while(arreglo[i] <= ancla && i < j) i++;
            while(arreglo[j] > ancla) j--;
            if (i < j) {
                aux= arreglo[i];
                arreglo[i]=arreglo[j];
                arreglo[j]=aux;
            }
        }

        arreglo[izq]=arreglo[j];
        arreglo[j]=ancla;

        if(izq < j-1) {
            quicksort(arreglo, izq, j - 1);
        }
        if(j+1 < der) {
            quicksort(arreglo, j + 1, der);
        }
        return arreglo;
    }
}
