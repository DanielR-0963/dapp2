#include <iostream>

/* UNIVERSIDAD IBEROAMERICANA
 * TSU en Software
 * Profesor:  Esp. Omar Vazquez Gonzalez
 * Alumno:       Ramírez Lázaro Luis Daniel
 * Asignatura:    Desarrollo de Aplicaciones II
 * Fecha:    23/01/2021
 * Programa:   Ejercicio 2
 * Descripción:   Programa que muestre las tablas de multiplicar del 1 al 10
*/


using namespace std;

void pasoPorValor(double radio);
void PasoPorReferencia(double &radio);

int main() {

    double radio = 6.5;

    pasoPorValor(radio);

    PasoPorReferencia(radio);

    return 0;
}

void pasoPorValor(double radio) {
    float total = 3.1416 * (radio  * radio);
    cout << "Area: " << total << "\n";
}

void PasoPorReferencia(double &radio) {
    float total = 3.1416 * (radio  * radio);
    cout << "Area: " << total << "\n";
}
