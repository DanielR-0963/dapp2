cmake_minimum_required(VERSION 3.17)
project(pregunta03)

set(CMAKE_CXX_STANDARD 14)

add_executable(pregunta03 main.cpp)