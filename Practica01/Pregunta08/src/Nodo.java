public class Nodo {
    private int num;
    private Nodo izq;
    private Nodo der;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Nodo getDer() {
        return der;
    }

    public void setDer(Nodo der) {
        this.der = der;
    }

    public Nodo getIzq() {
        return izq;
    }

    public void setIzq(Nodo izq) {
        this.izq = izq;
    }

    public Nodo(int num, Nodo izq, Nodo der) {
        this.num = num;
        this.der = der;
        this.izq = izq;
    }

    public Nodo(int num) {
        this.num = num;
        this.der = null;
        this.izq = null;
    }

    public static Nodo CrearArbol () {
        return new Nodo( 6,
                new Nodo(4,
                        new Nodo(2,
                                new Nodo(1),
                                new Nodo(3)),
                        new Nodo(5)),
                new Nodo(7,
                        null,
                        new Nodo(9,
                                new Nodo(8),
                                null)) );
    }

    public static Nodo buscarNodo (Nodo actual, int numBuscado) {
        Nodo nodoEncontrado = null;
        if (actual.getNum() == numBuscado) {
            return actual;

        } else {

            if (actual.getIzq() != null) {
                nodoEncontrado = buscarNodo(actual.getIzq(), numBuscado);
            }

            if (actual.getDer() != null && nodoEncontrado == null) {
                nodoEncontrado = buscarNodo(actual.getDer(), numBuscado);
            }

            return nodoEncontrado;
        }
    }

    public void inOrden () {
        if (getIzq() != null) {
            getIzq().inOrden();
        }
        System.out.println("Numero: " + getNum());
        if (getDer() != null) {
            getDer().inOrden();
        }
    }

    public void preOrden () {
        System.out.println("Numero: " + getNum());
        if(getIzq() != null) {
            getIzq().preOrden();
        }
        if (getDer() != null) {
            getDer().preOrden();
        }
    }

    public void posOrden () {
        if (getIzq() != null) {
            getIzq().posOrden();
        }
        if (getDer() != null){
            getDer().posOrden();
        }
        System.out.println("Numero: " + getNum());
    }
}
