import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Pregunta08 {
    public static void main(String[] args) {
        Nodo padre =  Nodo.CrearArbol();
        int opcion = 0;
        do {
            opcion = menu();
            switch (opcion) {
                case 1: //preOrden
                    padre.preOrden();
                    break;
                case 2: //InOrden
                    padre.inOrden();
                    break;
                case 3: //posOrden
                    padre.posOrden();
                    break;
                case 4: //Buscar
                    Nodo encontrado = Nodo.buscarNodo(padre, preguntar());
                    if (encontrado != null) {
                        System.out.println("Se encotro el nodo: " + encontrado.getNum());
                    } else {
                        System.out.println("Nodo no encontrado");
                    }
                    break;
                case 5: //Salir
                    System.out.println("Fin del programa");
                    break;
            }
        } while(opcion != 5);

    }

    private static int menu() {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int opcion;
        System.out.println("1. Imprimir en Pre-Orden");
        System.out.println("2. Imprimir en In-Orden");
        System.out.println("3. Imprimir en Pos-Orden");
        System.out.println("4. Buscar numero");
        System.out.println("5. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Problema al leer la opcion");
            opcion = 0;
        }
        return opcion;
    }

    private static int preguntar () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int numeroBuscado = 0;
        System.out.println("Que numero quieres buscar?");
        try {
            numeroBuscado = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Problema al leer tu numero");
        }
        return numeroBuscado;

    }

}
