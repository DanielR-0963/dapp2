public class Pregunta05 {

    public static void main(String[] args) {
        int[] x = new int[]{9, 6, 7, 2, 5, 3};

        int []resultado = metodoBurbuja(x);

        for (int i = 0; i < resultado.length; i++) {
            System.out.println(resultado[i]);
        }
    }

    private static int[] metodoBurbuja(int[] x) {
        int temp = 0;
        for (int i = 0; i < x.length - 1; ++i) {
            for (int j = 0; j < x.length -1 - i; ++j) {
                if (x[j] > x[j+1]) {
                    temp = x[j];
                    x[j] = x[j+1];
                    x[j + 1] = temp;
                }
            }
        }
        return x;
    }
}
