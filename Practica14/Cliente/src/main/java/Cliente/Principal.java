package Cliente;

import Cliente.Controller.PrincipalController;
import Cliente.Entity.Gestores.GestorPersonas;
import Cliente.Entity.Persona;
import Cliente.Entity.Service.PersonaService;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;


public class Principal {
    public static void main(String[] args) {
        Integer puerto = 8080, opcion = 0;
        String host = "127.0.0.1";
        Gson gson = new Gson();

        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = null;
        HttpEntity entity = null;
        String result = null;
        Persona persona;

        try {
            do {
                opcion = PrincipalController.menu();
                switch (opcion) {
                    case 1: //mostrar
                        HttpGet get = new HttpGet("http://localhost:8080/get");
                        response = client.execute(get);
                        entity = response.getEntity();
                        result = EntityUtils.toString(entity);

                        GestorPersonas personas = gson.fromJson(result, GestorPersonas.class);

                        for (Persona iterador : personas.getPersonas()) {
                            System.out.println(iterador.toString());
                        }
                        break;
                    case 2: // insertar (post)
                        persona = PersonaService.cuestionario();
                        HttpPost post = new HttpPost("http://localhost:8080");
                        StringEntity stringEntity = new StringEntity(gson.toJson(persona));
                        post.setEntity(stringEntity);

                        post.setHeader("Content-type", "application/json");
                        response = client.execute(post);
                        entity = response.getEntity();
                        result = EntityUtils.toString(entity);

                        System.out.println(result);
                        break;
                    case 3: // actualizar (put)
                        persona = actualizar(PersonaService.preguntarID(), client);

                        HttpPut put = new HttpPut("http://localhost:8080");

                        StringEntity stringEntity1 = new StringEntity(gson.toJson(persona));
                        put.setEntity(stringEntity1);

                        put.setHeader("Content-type", "application/json");
                        response = client.execute(put);
                        entity = response.getEntity();
                        result = EntityUtils.toString(entity);

                        System.out.println(result);
                        break;
                    case 4: // Eliminar (delete)
                        Integer id = PersonaService.preguntarID();
                        String uri = "http://localhost:8080/" + id;
                        HttpDelete delete = new HttpDelete(uri);

                        response =  client.execute(delete);
                        entity = response.getEntity();
                        result = EntityUtils.toString(entity);

                        System.out.println(result);
                        break;
                    case 5: // Salir
                        System.out.println("Fin del programa");
                        break;
                    default:
                        System.out.println("Opcion no valida");
                        break;
                }

            }while (opcion != 5);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static Persona actualizar (Integer id, HttpClient client) {
        Gson gson = new Gson();
        Persona personaAux = null;
        try {
            HttpGet get2 = new HttpGet("http://localhost:8080/get");
            HttpResponse response = client.execute(get2);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);


            GestorPersonas aux = gson.fromJson(result, GestorPersonas.class);

            for (Persona iterador : aux.getPersonas()) {
                if (iterador.getId_persona().equals(id)) {
                    personaAux = iterador;
                    break;
                }
            }
            if (personaAux != null) {
                PersonaService.actualizar(personaAux);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return personaAux;
    }
}
