package Cliente.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrincipalController {

    public static Integer menu () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        Integer opcion = 0;
        System.out.println("1. Mostrar lista de personas");
        System.out.println("2. Insertar nueva persona");
        System.out.println("3. Actualizar persona");
        System.out.println("4. Eliminar persona");
        System.out.println("5. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }
}
