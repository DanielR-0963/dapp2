package Cliente.Entity.Gestores;

import Cliente.Entity.Persona;

import java.util.ArrayList;

public class GestorPersonas {
    ArrayList<Persona> personas;

    public ArrayList<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }

    public GestorPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }

    public GestorPersonas() {
    }

    @Override
    public String toString() {
        return "GestorPersonas{" +
                "personas=" + personas +
                '}';
    }
}
