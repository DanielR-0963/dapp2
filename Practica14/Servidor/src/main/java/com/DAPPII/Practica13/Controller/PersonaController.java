package com.DAPPII.Practica13.Controller;

import com.DAPPII.Practica13.Model.Gestores.GestorPersonas;
import com.DAPPII.Practica13.Model.Persona;
import com.DAPPII.Practica13.Service.PersonaService;
import com.google.gson.Gson;
import org.omg.CORBA.INTERNAL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RestController
public class PersonaController {

    @Autowired
    private PersonaService service;
    private Gson gson = new Gson();

    @GetMapping("/get")
    public String listarPersonas() {
        GestorPersonas gestorPersonas = new GestorPersonas(service.listarPersona());
        return gson.toJson(gestorPersonas);
    }


    @PostMapping(value = "/", consumes = "application/json", produces = "aplication/json")
    public String nuevaPersona(@RequestBody Persona persona) {
        service.save(persona);
        return "Insercion exitosa";
    }


    @DeleteMapping(value = "/{id}")
    public String eliminar (@PathVariable("id") Integer id) {
        service.deleteByID(id);
        return "Eliminacion exitosa";
    }


    @PutMapping(value = "/",consumes = "application/json", produces = "aplication/json")
    public String actualizar (@RequestBody Persona persona) {
        service.save(persona);
        return "Actualizacion exitosa";
    }
}
