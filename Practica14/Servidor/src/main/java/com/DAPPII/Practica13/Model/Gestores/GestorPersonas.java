package com.DAPPII.Practica13.Model.Gestores;

import com.DAPPII.Practica13.Model.Persona;

import java.util.ArrayList;
import java.util.List;

public class GestorPersonas {
    List<Persona> personas;

    public GestorPersonas() {
    }

    public GestorPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
}
