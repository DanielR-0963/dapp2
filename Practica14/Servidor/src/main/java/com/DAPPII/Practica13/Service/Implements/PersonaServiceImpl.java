package com.DAPPII.Practica13.Service.Implements;

import com.DAPPII.Practica13.Model.Persona;
import com.DAPPII.Practica13.Repository.PersonaRepository;
import com.DAPPII.Practica13.Service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepository serviceCRUD;

    @Override
    public List<Persona> listarPersona() {
        return (List<Persona>) serviceCRUD.findAll();
    }

    @Override
    public void save(Persona persona) {
        serviceCRUD.save(persona);
    }

    @Override
    public Persona findByID(Integer id) {
        return serviceCRUD.findById(id).orElse(null);
    }

    @Override
    public void deleteByID(Integer id) {
        serviceCRUD.deleteById(id);
    }
}
