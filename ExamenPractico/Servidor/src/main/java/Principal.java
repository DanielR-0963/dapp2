import DB.DB;
import Entity.Auto;
import Entity.Service.AutoService;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;

public class Principal {
    public static void main(String[] args) {
        Integer puerto = 8000;
        Socket miCliente = null;
        ServerSocket miServer = null;
        String mensajeRecibido = "", mensajeEnviado = "";
        BufferedReader reader = null;
        InputStreamReader input = null;
        PrintWriter writer = null;
        Gson gson = new Gson();
        Connection connection = null;

        try {
            miServer = new ServerSocket(puerto);
            System.out.println("Esperando conexion...");
            miCliente = miServer.accept();
            System.out.println("Conexion establecida");

            input = new InputStreamReader(miCliente.getInputStream());
            reader = new BufferedReader(input);
            writer = new PrintWriter(miCliente.getOutputStream(), true);

            connection = DB.Conectar();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            do {
                mensajeRecibido = reader.readLine();
                switch (mensajeRecibido) {
                    case "GET":
                        System.out.println("Se entro al get");
                        mensajeEnviado = AutoService.toArrayXML(AutoService.select(connection));
                        System.out.println(mensajeEnviado);
                        writer.println(mensajeEnviado);
                        break;
                    case "POST":
                        System.out.println("Se entro al POST");
                        String datos = reader.readLine();
                        Auto auto = gson.fromJson(datos, Auto.class);
                        System.out.println(auto.toString());
                        AutoService.insert(connection, auto);
                        break;
                    default:
                        System.out.println("Opcion no valida");
                        break;
                }
            } while (!mensajeRecibido.equals("SALIR"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
