package Entity.Service;

import Entity.Auto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AutoService {
    public static ArrayList<Auto> select (Connection connection) {
        ArrayList<Auto> autos = new ArrayList<Auto>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String sql = "select model, name, color, kilometers, position from auto";
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Auto auto = new Auto();
                auto.setModel(resultSet.getString(1));
                auto.setName(resultSet.getString(2));
                auto.setColor(resultSet.getString(3));
                auto.setKilometers(resultSet.getDouble(4));
                auto.setPosition(resultSet.getDouble(5));
                autos.add(auto);
                auto = null;
            }

            statement.close();
            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return autos;
    }

    public static String toXML (Auto auto) {
        String xml = "<auto>";

        xml += "<model>" + auto.getModel() + "</model>";
        xml += "<name>" + auto.getName() + "</name>";
        xml += "<color>" + auto.getColor() + "</color>";
        xml += "<kilometers>" + auto.getKilometers() + "</kilometers>";
        xml += "<position>" + auto.getPosition() + "</position>";

        xml += "</auto>";
        return xml;
    }

    public static String toArrayXML (ArrayList<Auto> list) {
        String xml = "<autos>";

        for (Auto iter: list) {
            xml += toXML(iter);
        }
        xml += "</autos>";
        return xml;
    }

    public static void insert (Connection connection, Auto auto) {
        PreparedStatement statement = null;
        String sql = "insert into auto(model, name, color, kilometers, position) VALUES (?, ?, ?, ?, ?)";

        try {
            statement = connection.prepareStatement(sql);

            statement.setString(1, auto.getModel());
            statement.setString(2, auto.getColor());
            statement.setString(3, auto.getColor());
            statement.setDouble(4, auto.getKilometers());
            statement.setDouble(5, auto.getPosition());

            statement.executeUpdate();
            statement.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
