package Entity.Service;

import Entity.Auto;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AutoService {
    public static ArrayList<Auto> fromSML(NodeList nodeList) {
        ArrayList<Auto> autos = new ArrayList<Auto>();
        NodeList hijos;
        Node nodo, hijo;
        Element element;
        for (int i = nodeList.getLength() - 1; i >=0; i--) {
            nodo = nodeList.item(i);
            if (nodo.getNodeType() == Node.ELEMENT_NODE) {
                element = (Element) nodo;
                hijos = element.getChildNodes();
                Auto auto = new Auto();
                for (int j = 0; j < hijos.getLength(); j++) {
                    hijo = hijos.item(j);
                    if (hijo.getNodeType() == Node.ELEMENT_NODE){
                        switch (hijo.getNodeName()) {
                            case "model":
                                auto.setModel(hijo.getTextContent());
                                break;
                            case "name":
                                auto.setName(hijo.getTextContent());
                                break;
                            case "color":
                                auto.setColor(hijo.getTextContent());
                                break;
                            case "kilometers":
                                auto.setKilometers(Double.parseDouble(hijo.getTextContent()));
                                break;
                            case "position":
                                auto.setPosition(Double.parseDouble(hijo.getTextContent()));
                                break;
                        }
                    }
                }
                autos.add(auto);
                auto = null;
            }
        }
        return autos;
    }

    public static Auto cuestionario () {
        Auto auto = new Auto();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Model: ");
            auto.setModel(reader.readLine());
            System.out.println("Name: ");
            auto.setName(reader.readLine());
            System.out.println("Color:");
            auto.setColor(reader.readLine());
            System.out.println("Kilometers: ");
            auto.setKilometers(Double.parseDouble(reader.readLine()));
            System.out.println("Position: ");
            auto.setPosition(Double.parseDouble(reader.readLine()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return auto;
    }
}
