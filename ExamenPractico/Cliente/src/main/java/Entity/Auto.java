package Entity;

public class Auto {
    private String model;
    private String name;
    private String color;
    private Double kilometers;
    private Double position;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getKilometers() {
        return kilometers;
    }

    public void setKilometers(Double kilometers) {
        this.kilometers = kilometers;
    }

    public Double getPosition() {
        return position;
    }

    public void setPosition(Double position) {
        this.position = position;
    }

    public Auto() {
    }

    public Auto(String model, String name, String color, Double kilometers, Double position) {
        this.model = model;
        this.name = name;
        this.color = color;
        this.kilometers = kilometers;
        this.position = position;
    }

    @Override
    public String toString() {
        return "Auto{" +
                "model='" + model + '\'' +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", kilometers=" + kilometers +
                ", position=" + position +
                '}';
    }
}
