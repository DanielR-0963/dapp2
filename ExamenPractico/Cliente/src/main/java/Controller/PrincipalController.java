package Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PrincipalController {
    public static Integer menu() {
        Integer opcion = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. Inciso D");
        System.out.println("2. Inciso E");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }
}
