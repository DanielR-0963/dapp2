import Controller.PrincipalController;
import Entity.Auto;
import Entity.Service.AutoService;
import com.google.gson.Gson;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Principal {
    public static void main(String[] args) {
        Integer puerto = 8000, opcion = 0;
        String host = "127.0.0.1", mensajeRecibido = "";
        Socket miCliente;
        InputStreamReader input = null;
        BufferedReader reader = null;
        PrintWriter writer = null;
        Document doc;
        NodeList nodeList;
        Gson gson = new Gson();
        try {
            miCliente = new Socket(host, puerto);
            input = new InputStreamReader(miCliente.getInputStream());
            reader = new BufferedReader(input);
            writer = new PrintWriter(miCliente.getOutputStream(), true);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            do {
                opcion = PrincipalController.menu();
                switch (opcion) {
                    case 1: // GET
                        writer.println("GET");
                        mensajeRecibido = reader.readLine();
                        doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
                                new ByteArrayInputStream(mensajeRecibido.getBytes()));
                        nodeList = doc.getElementsByTagName("auto");
                        ArrayList<Auto> autos = AutoService.fromSML(nodeList);
                        for (Auto iter : autos) {
                            System.out.println(iter.toString());
                        }
                        break;
                    case 2: // POST
                        writer.println("POST");
                        Auto auto = AutoService.cuestionario();
                        String mensajeEnviado = gson.toJson(auto, Auto.class);
                        System.out.println(mensajeEnviado);
                        writer.println(mensajeEnviado);
                        break;
                    case 3:
                        break;
                    default:
                        System.out.println("Opcion no valida");
                        break;
                }

            } while (opcion != 3);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }

    }
}
