package com.DAPPII.Practica13.Controller;

import com.DAPPII.Practica13.Model.Persona;
import com.DAPPII.Practica13.Service.PersonaService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/personas")
public class PersonaController {

    @Autowired
    private PersonaService service;
    private Gson gson = new Gson();

    @GetMapping ("/lista")
    public String listarPersonas(Model model) {
        List<Persona> personas = service.listarPersona();
        model.addAttribute("personas", personas);
        return "/html/TablaPersona";
    }

    @RequestMapping(value = "/cuestionario", method = RequestMethod.GET)
    public String cuestionario (Model model) {
        Persona persona = new Persona();

        model.addAttribute("persona", persona);
        return "/html/Cuestionario";
    }

    @PostMapping("/guardar")
    public String nuevaPersona(@ModelAttribute Persona persona) {
        service.save(persona);
        return "redirect:/personas/lista";
    }

    @RequestMapping(value = "/actualizar/{id}", method = RequestMethod.GET)
    public String actualizar (@PathVariable("id") Integer id, Model model) {
        Persona persona = service.findByID(id);
        model.addAttribute("persona", persona);
        return "/html/Cuestionario";
    }

    /*@RequestMapping(value = "/eliminar/{id}", method = RequestMethod.GET)
    public String eliminar (@PathVariable("id") Integer id) {
        service.deleteByID(id);
        return "redirect:/personas/lista";
    }*/

    @DeleteMapping(value = "/{id}")
    public void eliminar (@PathVariable("id") Integer id) {
        service.deleteByID(id);
    }

    /*@PutMapping(value = "/{id}")
    public void actualizar (@PathVariable("id") Integer id) {
        Persona persona = service.findByID(id);

        persona.setNombre(persona.getNombre());
        persona.setPaterno(persona.getPaterno());
        persona.setMaterno(persona.getMaterno());
        persona.setEdad(persona.getEdad());
        persona.setGenero(persona.getGenero());

    }*/
}
