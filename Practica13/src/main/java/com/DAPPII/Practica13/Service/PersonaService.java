package com.DAPPII.Practica13.Service;

import com.DAPPII.Practica13.Model.Persona;

import java.util.List;

public interface PersonaService {
    public List<Persona> listarPersona();
    public void save (Persona persona);
    public Persona findByID (Integer id);
    public void deleteByID(Integer id);
}
