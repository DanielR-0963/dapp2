package com.DAPPII.Practica13.Repository;

import com.DAPPII.Practica13.Model.Persona;
import org.springframework.data.repository.CrudRepository;


public interface PersonaRepository extends CrudRepository<Persona, Integer> {
}
