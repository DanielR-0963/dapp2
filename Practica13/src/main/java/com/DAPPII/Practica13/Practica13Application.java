package com.DAPPII.Practica13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class Practica13Application {

    public static void main(String[] args) {
        SpringApplication.run(Practica13Application.class, args);
     }

}
