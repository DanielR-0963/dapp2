package Cliente;

import Cliente.Entity.Persona;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Principal {
    public static void main(String[] args) {

        String mensajeEnviado;
        String host = "127.0.0.1";
        int puerto = 8000;
        Socket miCliente;
        PrintWriter printWriter = null;

        Persona persona = nuevaPersona();
        mensajeEnviado = persona.toXML();

        try {
            miCliente = new Socket(host, puerto);
            printWriter = new PrintWriter(miCliente.getOutputStream(), true);
            printWriter.println(mensajeEnviado);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(persona.toXML());
    }

    private static Persona nuevaPersona () {
        Persona persona = new Persona();
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        try {
            persona.setId(0);
            System.out.println("Nombre:");
            persona.setNombre(scan.readLine());

            System.out.println("Apellido paterno:");
            persona.setPaterno(scan.readLine());

            System.out.println("Apellido materno:");
            persona.setMaterno(scan.readLine());

            System.out.println("Edad:");
            persona.setEdad(Integer.parseInt(scan.readLine()));

            System.out.println("Genero(M/F):");
            persona.setGenero(scan.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return persona;
    }
}
