package Servidor.Entity;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Persona {
    private int id;
    private String nombre;
    private String paterno;
    private String materno;
    private int edad;
    private String genero;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getPaterno() {
        return paterno;
    }
    public void setPaterno(String paterno) {
        this.paterno = paterno;
    }
    public String getMaterno() {
        return materno;
    }
    public void setMaterno(String materno) {
        this.materno = materno;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public String getGenero() {
        return genero;
    }
    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Persona(int id, String nombre, String paterno, String materno, int edad, String genero) {
        this.id = id;
        this.nombre = nombre;
        this.paterno = paterno;
        this.materno = materno;
        this.edad = edad;
        this.genero = genero;
    }
    public Persona() {
    }

    @Override
    public String toString() {
        return "Persona{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", paterno='" + paterno + '\'' +
                ", materno='" + materno + '\'' +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                '}';
    }

    public void fromXML (NodeList nodeList) {
        NodeList hijos;
        Node nodo, hijo;
        Element element;
        nodo = nodeList.item(0);
        if (nodo.getNodeType() == Node.ELEMENT_NODE) {
            element = (Element) nodo;
            hijos = element.getChildNodes();
            for (int j = 0; j < hijos.getLength(); j++) {
                hijo = hijos.item(j);
                if(hijo.getNodeType() == Node.ELEMENT_NODE) {
                    switch (hijo.getNodeName()) {
                        case "nombre":
                            setNombre(hijo.getTextContent());
                            break;
                        case "paterno":
                            setPaterno(hijo.getTextContent());
                            break;
                        case "materno":
                            setMaterno(hijo.getTextContent());
                            break;
                        case "edad":
                            setEdad(Integer.parseInt(hijo.getTextContent()));
                            break;
                        case "genero":
                            setGenero(hijo.getTextContent());
                            break;
                    }
                }
            }
        }
    }
}
