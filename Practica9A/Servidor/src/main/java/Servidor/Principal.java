package Servidor;

import Servidor.DB.DB;
import Servidor.Entity.Persona;
import Servidor.Entity.PersonaService;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;

public class Principal {
    public static void main(String[] args) {
        int puerto = 8000;
        Socket miCliente = null;
        ServerSocket miServidor = null;
        InputStreamReader streamSocket = null;
        BufferedReader bufferedReader = null;
        byte[] mensajeRecibido;
        Document doc;
        NodeList nodeList;
        Persona persona = new Persona();
        Connection connection;
        try {
            miServidor = new ServerSocket(puerto);
            System.out.println("Espeando conexion...");
            miCliente = miServidor.accept();

            streamSocket = new InputStreamReader(miCliente.getInputStream());
            bufferedReader = new BufferedReader(streamSocket);

            System.out.println("Esperando el mensaje...");

            String prueba = bufferedReader.readLine();

            mensajeRecibido = prueba.getBytes();

            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
                    new ByteArrayInputStream(mensajeRecibido));

            nodeList = doc.getElementsByTagName("persona");

            persona.fromXML(nodeList);

            System.out.println(persona.toString());

            connection = DB.Conectar();

            PersonaService.insertarPersona(connection, persona);

        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
    }
}
