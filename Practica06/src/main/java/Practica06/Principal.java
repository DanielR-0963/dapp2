package Practica06;

import com.google.gson.Gson;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;

public class Principal {

    public static void main(String[] args) {
        int opcionPrim = 0, opcionSec = 0;
        Auto auto = new Auto("Ford", 1908, "T");
        do {
            opcionPrim = menuPrim();
            switch (opcionPrim) {
                case 1: //XML
                    do {
                        opcionSec = menuSec();
                        switch (opcionSec) {
                            case 1: //Imprimir
                                try {
                                    System.out.println("Se crea un archivo con los datos del objeto en xml");
                                    XMLEncoder xml = new XMLEncoder(new FileOutputStream("Autos.xml"));
                                    xml.writeObject(auto);

                                    xml.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 2: //convertir
                                System.out.println("Se lee el archivo con los datos xml y los muestra");
                                try {
                                    XMLDecoder xml = new XMLDecoder(new FileInputStream("Autos.xml"));
                                    Auto auto1 = (Auto) xml.readObject();
                                    System.out.println(auto1.toString());

                                    xml.close();
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 3: //salir
                                System.out.println("Volviendo al menu principal");
                                break;
                            default:
                                break;
                        }
                    } while (opcionSec != 3);
                    break;
                case 2: //JSON
                    Gson gson = new Gson();
                    do {
                        opcionSec = menuSec();
                        switch (opcionSec) {
                            case 1: //Imprimir
                                System.out.println(auto.toString());
                                System.out.println(gson.toJson(auto));
                                break;
                            case 2: //convertir
                                String nuevoAuto = "{" +
                                        "\"marca\": \"Suzuki\"," +
                                        "\"year\": 2012," +
                                        "\"modelo\": \"MT500\"" +
                                        "}";
                                Auto auto1 = gson.fromJson(nuevoAuto, Auto.class);
                                System.out.println(auto.toString());
                                System.out.println(gson.toJson(auto1));
                                auto1 = null;
                                break;
                            case 3: //salir
                                System.out.println("Volviendo al menu principal");
                                break;
                            default:
                                break;
                        }
                    } while (opcionSec != 3);
                    gson = null;
                    break;
                case 3: //salir
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("Opcion no valida");
            }
        } while (opcionPrim != 3);

    }

    public static int menuPrim () {
        int opcion = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. XML");
        System.out.println("2. JSON");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }

    public static int menuSec() {
        int opcion = 0;
        BufferedReader scan  = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. Imprimir");
        System.out.println("2. Convertir");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }
}
