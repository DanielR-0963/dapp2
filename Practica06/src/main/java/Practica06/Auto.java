package Practica06;

public class Auto {
    private String marca;
    private Integer year;
    private String modelo;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Auto(String marca, Integer year, String modelo) {
        this.marca = marca;
        this.year = year;
        this.modelo = modelo;
    }

    public Auto() {}

    @Override
    public String toString() {
        return "Auto{" +
                "marca='" + marca + '\'' +
                ", year=" + year +
                ", modelo='" + modelo + '\'' +
                '}';
    }
}
