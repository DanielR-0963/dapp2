package Practica07;

import java.io.*;

public class Principal {
    public static void main(String[] args) {
        int opcion = 0;
        String leido = "";
        BufferedReader scan;
        FileWriter file;
        do {
            opcion = menu();
            switch (opcion) {
                case 1: // leer
                    try {
                        scan = new BufferedReader(new FileReader("C:\\Users\\bizan\\Desktop\\Escritorio" +
                                "\\Escuela\\Carrera\\Gitlab\\CuartoSemestre\\dapp2\\Practica07\\src\\Practica07\\" +
                                "Archivo\\archivo.txt"));
                        leido = scan.readLine();
                        scan.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.println(leido);
                    break;
                case 2: //escribir
                    try {
                        file = new FileWriter("C:\\Users\\bizan\\Desktop\\Escritorio\\Escuela\\Carrera\\Gitlab" +
                                "\\CuartoSemestre\\dapp2\\Practica07\\src\\Practica07\\Archivo\\archivo.txt");
                        PrintWriter writer = new PrintWriter(file);
                        writer.println(preguntar(leido));
                        writer.close();
                        file.close();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 3: // salir
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        } while (opcion != 3);

    }

    public static int menu () {
        int opcion = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. Leer archivo");
        System.out.println("2. Escribir archivo");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }

    public static String preguntar (String frase) {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Que quieres escribir en tu archivo?");
        try {
            frase += scan.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return frase;
    }
}

