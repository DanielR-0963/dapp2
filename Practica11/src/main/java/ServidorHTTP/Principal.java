package ServidorHTTP;

import ServidorHTTP.GestorArchivos.GestorArchivos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Principal {
    public static void main(String[] args) {
        int puerto = 8000;
        int content = 0;
        ServerSocket miServidor;
        Socket miCliente;
        String mensajeEnviado = "", mensajeRecibido = "";
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        PrintWriter printWriter = null;
        String pagina = GestorArchivos.leerArchivo("C:\\Users\\bizan\\Desktop\\Escritorio\\Escuela\\Carrera" +
                "\\Gitlab\\CuartoSemestre\\dapp2\\Practica11\\src\\main\\resources\\HTML\\pagina.html");
        System.out.println(pagina);

        try {
            miServidor = new ServerSocket(puerto);
            miCliente = miServidor.accept();

            inputStreamReader = new InputStreamReader(miCliente.getInputStream());
            bufferedReader = new BufferedReader(inputStreamReader);
            printWriter = new PrintWriter(miCliente.getOutputStream(), true);

            mensajeEnviado = "HTTP/1.1 200 OK\n";
            mensajeEnviado += "Content-Type:text/html\n";
            mensajeEnviado += "Content-length:" + pagina.length() + "\n\n";
            mensajeEnviado += pagina;

            System.out.println("Esperando mensaje...");
            mensajeRecibido = bufferedReader.readLine();
            System.out.println(mensajeRecibido);

            System.out.println("Enviando mensaje...");
            System.out.println(mensajeEnviado);
            printWriter.println(mensajeEnviado);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
