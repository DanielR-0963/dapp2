package ServidorHTTP.GestorArchivos;

import java.io.*;

public class GestorArchivos {
    public static String leerArchivo (String url) {
        String stringFinal = "";
        String linea = "";
        File archivo = null;
        try {
            archivo = new File(url);
            BufferedReader reader = new BufferedReader(new FileReader(archivo));
            while ((linea = reader.readLine()) != null) {
                stringFinal += linea;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringFinal;
    }
}
