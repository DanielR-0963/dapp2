package Cliente;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Principal {
    public static void main(String[] args) throws IOException {
        String mensajeEnviado = "", host = "127.0.0.1";
        int puerto = 8000;
        PrintWriter printWriter = null;
        Socket miCliente;
        InputStreamReader streamSocket = null;
        BufferedReader bufferedReader = null;
        String mensajeRecibido = "";
        try {
            miCliente = new Socket(host, puerto);
            streamSocket = new InputStreamReader(miCliente.getInputStream());
            bufferedReader = new BufferedReader(streamSocket);
            printWriter = new PrintWriter(miCliente.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        do {
            System.out.print("Cliente (Tu): ");
            mensajeEnviado = chat();
            if (printWriter != null) {
                printWriter.println(mensajeEnviado);
                if (!mensajeEnviado.equals("Adios")) {
                    System.out.print("Servidor: ");
                    mensajeRecibido = bufferedReader.readLine();
                    System.out.println(mensajeRecibido);
                }
            } else {
                mensajeEnviado = "Adios";
                System.out.println("No se a podido realizar la conexion");
            }
        } while (!mensajeEnviado.equals("Adios"));
    }
    private static String chat () {
        String mensajeEnviado = "";
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        try {
            mensajeEnviado = scan.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mensajeEnviado;
    }
}
