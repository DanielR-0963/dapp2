package Servidor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Principal {
    public static void main(String[] args) {
        int puerto = 8000;
        Socket miCliente = null;
        ServerSocket miServidor = null;
        InputStreamReader streamSocket = null;
        BufferedReader bufferedReader = null;
        String mensajeRecibido = "", mensajeEnviado = "";
        PrintWriter printWriter = null;
        try {
            miServidor = new ServerSocket(puerto);
            System.out.println("Esperando conexion...");
            miCliente = miServidor.accept();
            streamSocket = new InputStreamReader(miCliente.getInputStream());
            bufferedReader = new BufferedReader(streamSocket);
            printWriter = new PrintWriter(miCliente.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        do {
            try {
                System.out.print("Cliente: ");
                mensajeRecibido = bufferedReader.readLine();
                System.out.println(mensajeRecibido);
                if (!mensajeRecibido.equals("Adios")) {
                    System.out.print("Servidor (Tu): ");
                    mensajeEnviado = chat();
                    printWriter.println(mensajeEnviado);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!mensajeRecibido.equals("Adios"));
    }

    private static String chat () {
        String mensajeEnviado = "";
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        try {
            mensajeEnviado = scan.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mensajeEnviado;
    }
}
