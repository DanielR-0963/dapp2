package Practica02.CRUD;

import Practica02.CRUD.DataBase.MySQL;
import Practica02.CRUD.Objects.Alumno;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;

public class Principal {
    public static void main(String[] args) {
        int opcion = 0;
        Connection connection = MySQL.conectar();
        do{
            opcion = menu();
            switch (opcion) {
                case 1: //Create
                    Alumno.nuevoAlumno(connection, preguntasNuevoAlumno());
                    break;
                case 2: //Read
                    for (Alumno iterador : Alumno.crearListaEmpleados(connection)) {
                        System.out.println(iterador.toString());
                    }
                    break;
                case 3: // Update
                    int opcionSec = preguntarActualizacion();
                    switch (opcionSec) {
                        case 1: //Nombre
                            Alumno.actulizarNombre(connection, preguntarCambio(), preguntarID());
                            break;
                        case 2://Paterno
                            Alumno.actulizarPaterno(connection, preguntarCambio(), preguntarID());
                            break;
                        case 3: //Materno
                            Alumno.actulizarMaterno(connection, preguntarCambio(), preguntarID());
                            break;
                        case 4: // Grupo
                            Alumno.actulizarGrupo(connection, preguntarCambio(), preguntarID());
                            break;
                        default:
                            System.out.println("Fallo  en la actualizacion");
                            break;
                    }
                    break;
                case 4: // Delete
                    Alumno.eliminarAlumno(connection, preguntarID());
                    break;
                case 5: //Salir
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        }while (opcion != 5);
    }

    private static int menu () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int opcion = 0;
        System.out.println("1. Agreagar Alumno");
        System.out.println("2. Mostrar Alumnos");
        System.out.println("3. Actualizar Alumno");
        System.out.println("4. Eliminar alumno");
        System.out.println("5. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }

    private static int preguntarID () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        int id = 0;
        System.out.println("Que alumno buscas? (Coloque ID)");
        try {
            id = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return id;
    }

    private static String preguntarCambio () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        String nuevo = "";
        System.out.println("Nuevo valor a actualizar: ");
        try {
            nuevo = scan.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nuevo;
    }

    private static int preguntarActualizacion () {
        int opcion = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Que parte quieres actualizar?");
        System.out.println("1. Nombre");
        System.out.println("2. Apellido Paterno");
        System.out.println("3. Apellido Materno");
        System.out.println("4. Grupo");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }

    private static Alumno preguntasNuevoAlumno () {
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        Alumno nuevoAlumno = new Alumno();
        try {
            System.out.println("Nombre: ");
            nuevoAlumno.setNombre(scan.readLine());

            System.out.println("Apellido paterno: ");
            nuevoAlumno.setApellidoPaterno(scan.readLine());

            System.out.println("Apellido materno: ");
            nuevoAlumno.setApellidoMaterno(scan.readLine());

            System.out.println("Grupo: ");
            nuevoAlumno.setGrupo(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nuevoAlumno;
    }
}
