package Practica02.CRUD.Objects;

import Practica02.CRUD.DataBase.MySQL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Alumno {
    private int id;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String grupo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public Alumno() {
    }

    public Alumno(int id, String nombre, String apellidoPaterno, String apellidoMaterno, String grupo) {
        this.id = id;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.grupo = grupo;
    }

    @Override
    public String toString() {
        return "Alumno{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", apellidoPaterno='" + apellidoPaterno + '\'' +
                ", apellidoMaterno='" + apellidoMaterno + '\'' +
                ", grupo='" + grupo + '\'' +
                '}';
    }

    public static ArrayList<Alumno> crearListaEmpleados(Connection connection) {
        ArrayList<Alumno> empleados = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet relset = null;
        String sql = "select * from alumno";
        try {
            statement = MySQL.consulta(connection, sql);
            relset = statement.executeQuery();
            while (relset.next()) {

                Alumno  empleadoAux = new Alumno(relset.getInt(1), relset.getString(2),
                        relset.getString(3), relset.getString(4), relset.getString(5));

                empleados.add(empleadoAux);

                empleadoAux = null;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (relset != null) {
                    relset.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return empleados;
    }

    public static void actulizarNombre (Connection connection, String nuevo, int id) {
        PreparedStatement stm;
        String sql = "update Alumno set Nombre = ? where Matricula = " + id;
        try {
            stm = MySQL.consulta(connection, sql);
            stm.setString(1, nuevo);
            stm.executeUpdate();
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void actulizarPaterno (Connection connection, String nuevo, int id) {
        PreparedStatement stm;
        String sql = "update Alumno set Apellido_paterno = ? where Matricula = " + id;
        try {
            stm = MySQL.consulta(connection, sql);
            stm.setString(1, nuevo);
            stm.executeUpdate();
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void actulizarMaterno (Connection connection, String nuevo, int id) {
        PreparedStatement stm;
        String sql = "update Alumno set Apellido_materno = ? where Matricula = " + id;
        try {
            stm = MySQL.consulta(connection, sql);
            stm.setString(1, nuevo);
            stm.executeUpdate();
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void actulizarGrupo (Connection connection, String nuevo, int id) {
        PreparedStatement stm;
        String sql = "update Alumno set Grupo = ? where Matricula = " + id;
        try {
            stm = MySQL.consulta(connection, sql);
            stm.setString(1, nuevo);
            stm.executeUpdate();
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void eliminarAlumno (Connection connection, int id) {
        PreparedStatement stm;
        String sql = "delete from alumno where Matricula = " + id;
        try {
            stm = MySQL.consulta(connection, sql);
            stm.executeUpdate();
            stm.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static void nuevoAlumno (Connection connection, Alumno nuevoAlumno) {
        PreparedStatement stm;
        String sql = "insert into alumno (Matricula, Nombre, Apellido_paterno, Apellido_materno, Grupo) values (default, ?, ?, ? ,?)";
        try {
            stm = MySQL.consulta(connection,sql);
            stm.setString(1, nuevoAlumno.getNombre());
            stm.setString(2, nuevoAlumno.getApellidoPaterno());
            stm.setString(3, nuevoAlumno.getApellidoMaterno());
            stm.setString(4, nuevoAlumno.getGrupo());

            stm.executeUpdate();
            stm.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
