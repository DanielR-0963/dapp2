package Service;

import Entity.Persona;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class PersonaService {

    public ArrayList<Persona> listarPersonas(Connection connection) {
        ArrayList<Persona> personas = new ArrayList<Persona>();
        try {
            String sql = "SELECT id_persona, nombre, paterno, materno, edad, genero FROM persona";
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()){
                Persona persona = new Persona();
                persona.setId(rs.getInt(1));
                persona.setNombre(rs.getString(2));
                persona.setPaterno(rs.getString(3));
                persona.setMaterno(rs.getString(4));
                persona.setEdad(rs.getInt(5));
                persona.setGenero(rs.getString(6));
                personas.add(persona);
                persona = null;
            }
            stmt.close();
            rs.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return personas;
    }

    public ArrayList<Persona> toJson(Persona persona) {
        return null;
    }
}
