import DB.Conexion;
import Entity.Gestores.GestorPersonas;
import Service.PersonaService;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {
        int puerto = 8000;
        Gson gson = new Gson();
        GestorPersonas gestorPersonas = new GestorPersonas();
        Socket miCliente = null;
        ServerSocket miServidor;
        InputStreamReader streamSocket;
        BufferedReader lectorSocket;
        PrintWriter escritorSocket;
        String mensajeRecibido, mensajeEnviado;
        try {

            Connection connection = Conexion.conectar();
            PersonaService personaService = new PersonaService();
            gestorPersonas.setPersonas(personaService.listarPersonas(connection));

            miServidor = new ServerSocket(puerto);

            System.out.println("Esperando conexion");
            miCliente = miServidor.accept();
            System.out.println("Conexion aceptada");


            streamSocket = new InputStreamReader(miCliente.getInputStream());
            lectorSocket = new BufferedReader(streamSocket);
            escritorSocket = new PrintWriter(miCliente.getOutputStream(), true);

            System.out.println("Esperando mensaje...");
            mensajeRecibido = lectorSocket.readLine();
            System.out.println(mensajeRecibido);

            System.out.println("Mandando información...");

            mensajeEnviado = gson.toJson(gestorPersonas, GestorPersonas.class);

            escritorSocket.println(mensajeEnviado);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}