package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
    private static Connection conn2;

    public static Connection conectar() throws SQLException {
        conn2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/personas?useUnicode=true&useJDBC" +
                "CompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC"
                , "root", "Biz-0986");
        if (conn2 != null) {
            return conn2;
        } else {
            return null;
        }
    }
}
