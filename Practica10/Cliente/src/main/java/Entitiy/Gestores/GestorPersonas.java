package Entitiy.Gestores;

import Entitiy.Persona;

import java.util.ArrayList;

public class GestorPersonas {
    private ArrayList<Persona> personas;

    public ArrayList<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }

    public GestorPersonas(ArrayList<Persona> personas) {
        this.personas = personas;
    }

    public GestorPersonas() {
    }
}
