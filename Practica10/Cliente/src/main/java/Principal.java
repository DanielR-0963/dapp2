import Entitiy.Gestores.GestorPersonas;
import Entitiy.Persona;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Principal {
    public static void main(String[] args) {
        Socket miCliente = null;
        int puerto = 8000, opcion = 0;
        String host = "127.0.0.1", mensajeRecibido = "", transmitir;
        BufferedReader bufferedReader;
        InputStreamReader streamReader;
        do {
            opcion = menu();
            switch (opcion) {
                case 1: //Pedir personas
                    transmitir = "dame lista";
                    try {
                        miCliente = new Socket(host, puerto);
                        streamReader = new InputStreamReader(miCliente.getInputStream());
                        bufferedReader = new BufferedReader(streamReader);
                        PrintWriter printWriter = new PrintWriter(miCliente.getOutputStream(), true);
                        printWriter.println(transmitir);
                        mensajeRecibido = bufferedReader.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2: // imprimir
                    if (!mensajeRecibido.equals("")) {
                        Gson gson = new Gson();
                        GestorPersonas gestorPersonas = gson.fromJson(mensajeRecibido, GestorPersonas.class);
                        for (Persona persona : gestorPersonas.getPersonas()) {
                            System.out.println(persona.toString());
                        }
                    } else {
                        System.out.println("Lista no encontrada");
                    }
                    break;
                case 3: //Salir
                    System.out.println("Fin del programa");
                    break;
                default:
                    System.out.println("Opcion no valida");
                    break;
            }
        } while (opcion != 3);
    }

    private static int menu () {
        int opcion = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("1. Pedir personas");
        System.out.println("2. Imprimir personas");
        System.out.println("3. Salir");
        try {
            opcion = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return opcion;
    }
}
