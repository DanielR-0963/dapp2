package Dijkstra;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

public class CaminoMinimo {
    private int [][] Pesos;
    private int [] ultimo;
    private int [] D;
    private boolean [] F;
    private int s, n = 5; // vértice origen y número de vértices

    public CaminoMinimo(int origen) {
        s = origen;
        Pesos = cargaDesdeArchivo();
        ultimo = new int [n];
        D = new int [n];
        F = new boolean [n];
    }

    private int[][] cargaDesdeArchivo(){
        int[][] matrizAdy = new int[n][n];
        String nombAr;
        String a;
        StringTokenizer d;
        int f=0;
        int c=0;
        try{
            BufferedReader b = new  BufferedReader(new FileReader("C:\\Users\\bizan\\Desktop\\Escritorio" +
                    "\\Escuela\\Carrera\\Gitlab\\CuartoSemestre\\dapp2\\Practica03\\src\\Dijkstra\\Archivos" +
                    "\\Aeropuertos.txt"));
            while((a=b.readLine())!=null){
                d = new StringTokenizer(a);
                c=0;
                while(d.hasMoreTokens()) {
                    matrizAdy[f][c++]= Integer.parseInt(d.nextToken());
                }
                f++;
            }
        }
        catch(FileNotFoundException e){
            System.out.print("Error: "+e);
            System.exit(0);
        }
        catch(IOException e1){
            System.out.print("Error: "+e1);
            System.exit(0);
        }
        catch(NumberFormatException e2){
            System.out.print("Error: "+e2);
            System.exit(0);
        }
        return matrizAdy;
    }

    public void caminoMinimos() {
        for (int i = 0; i < n; i++) {
            F[i] = false;
            D[i] = Pesos[s][i];
            ultimo[i] = s;
        }
        F[s] = true; D[s] = 0;
        for (int i = 1; i < n; i++)
        {
            int v = minimo();
            F[v] = true;
            for (int w = 1; w < n; w++) {
                if (!F[w]) {
                    if ((D[v] + Pesos[v][w]) < D[w]) {
                        D[w] = D[v] + Pesos[v][w];
                        ultimo[w] = v;
                    }
                }
            }
        }
    }

    private int minimo() {
        int mx = 99999999;
        int v = 1;
        for (int j = 1; j < n; j++)
        {
            if (!F[j] && (D[j]<= mx))
            {
                mx = D[j];
                v = j;
            }
        }

        return v;
    }

    public void recuperaCamino(int v)
    {
        int anterior = ultimo[v];
        if (v != s) {
            recuperaCamino(anterior);
            System.out.print(" -> V" + v);
        }
        else {
            System.out.print("V" + s);
        }
    }
}

