package Dijkstra;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Principal {
    public static void main(String[] args) {
        System.out.println("Favor de revisar la docuemtnacion para usar este programa");
        CaminoMinimo cm = new CaminoMinimo(preguntar(1));
        cm.caminoMinimos();
        cm.recuperaCamino(preguntar(0));
    }

    private static int preguntar(int opcion) {
        String palabra;
        int nodo = 0;
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        if (opcion == 1){
            palabra = "origen";
        } else {
            palabra = "destino";
        }
        System.out.println("Cual es su nodo " + palabra + ": ");
        try {
            nodo = Integer.parseInt(scan.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nodo - 1;
    }
}

